import { Component } from '@angular/core';

@Component({
  selector: 'app-heroes-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.css']
})

export class HeroComponent {
  public showBtnName: boolean = true;
  public showBtnAge: boolean = true;

  public name: string = 'ironman';
  public age:  number = 45;

  get capitalizedName(): string {
    return this.name.toUpperCase();
  }

  getHeroDescription(): string {
    return  `${this.name} - ${this.age}` ;
  }

  changeHero(): void {
    this.name = 'Capitan America'
    this.showBtnName = false
  }

  changeAge(): void {
    this.age = 120
    this.showBtnAge = false
  }

  reset(): void {
    // document.querySelectorAll('h1')!.forEach( element => {
    //   element.innerHTML =  '<h1>Desde Angular</h1>';
    // });
    this.name = 'ironman';
    this.age = 45;
    this.showBtnAge = false;
    this.showBtnAge = this.showBtnName = true;
  }
}
