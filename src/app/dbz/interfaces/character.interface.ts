import { DbzService } from '../services/dbz.service';
export interface Character {
  id?: string,
  name: string;
  power: number;
}
